<?php

namespace Drupal\site_assistant\Services;

use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Context\ContextHandlerInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\site_assistant\Entity\Assistant;

/**
 * Class CustomService.
 */
class AssistantService {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The context handler.
   *
   * @var \Drupal\Core\Plugin\Context\ContextHandlerInterface
   */
  protected $contextHandler;

  /**
   * The context repository.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * The condition manager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $conditionManager;

  /**
   * AssistantService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Plugin\Context\ContextHandlerInterface $context_handler
   *   Context handler.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   Context repository.
   * @param \Drupal\Core\Condition\ConditionManager $condition_manager
   *   Condition manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ContextHandlerInterface $context_handler, ContextRepositoryInterface $context_repository, ConditionManager $condition_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->contextHandler = $context_handler;
    $this->contextRepository = $context_repository;
    $this->conditionManager = $condition_manager;
  }

  /**
   * Get the active assistant.
   *
   * @return \Drupal\site_assistant\Entity\Assistant|null
   *   A assist object or null if none evaluated for this request.
   */
  public function getActiveAssistant(): ?Assistant {
    $active_assistants = [];
    $active_assistants_weight = [];

    foreach ($this->getAllAssistants() as $assistant) {
      if ($this->assistSectionEvaluate($assistant)) {
        $active_assistants[$assistant->id()] = $assistant;
        $active_assistants_weight[$assistant->id()] = $assistant->getWeight();
      }
    }

    // Sort active assists by weight.
    asort($active_assistants_weight);

    $active_assistant_id = array_key_first($active_assistants_weight);

    return isset($active_assistants[$active_assistant_id]) ? $active_assistants[$active_assistant_id] : NULL;
  }

  /**
   * Get all assistants.
   *
   * @return \Drupal\site_assistant\AssistantInterface[]
   *   Array of assistants.
   */
  private function getAllAssistants(): iterable {
    $storage = $this->entityTypeManager->getStorage('assistant');

    return $storage->loadMultiple();
  }

  /**
   * Evaluate all condition plugins of this assistant.
   *
   * @param \Drupal\site_assistant\Entity\Assistant $assistant
   *   Assistant content.
   *
   * @return bool
   *   TRUE if all condition plugins evaluated true, FALSE if any evaluated
   *   false.
   */
  private function assistSectionEvaluate(Assistant $assistant) {
    $plugins_evaluated = [];

    /** @var \Drupal\Core\Condition\ConditionInterface $plugin */
    foreach ($assistant->getVisibilityPlugins() as $plugin) {

      // Inject runtime contexts if needed.
      if ($plugin instanceof ContextAwarePluginInterface) {
        $contexts = $this->contextRepository->getRuntimeContexts($plugin->getContextMapping());
        $this->contextHandler->applyContextMapping($plugin, $contexts);
      }

      $plugins_evaluated[] = $this->conditionManager->execute($plugin);
    }

    return !in_array(FALSE, $plugins_evaluated);
  }

}
