<?php

namespace Drupal\site_assistant\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\site_assistant\Services\AssistantService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block that shows the assist for this current page request.
 *
 * @Block(
 *   id = "site_assistant_assist_block",
 *   admin_label = @Translation("Site Assistant: Current assist")
 * )
 */
class SiteAssistantBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Assist service.
   *
   * @var \Drupal\site_assistant\Services\AssistantService
   */
  protected $assistantService;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * SiteAssistantBlock constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\site_assistant\Services\AssistantService $assistant_service
   *   Assist service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AssistantService $assistant_service, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->assistantService = $assistant_service;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('site_assistant.assistant_service'),
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['label_display' => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $renderable = [];
    $active_assistant = $this->assistantService->getActiveAssistant();

    if ($active_assistant) {
      $list_entries = [];

      $view_builder = $this->entityTypeManager->getViewBuilder('assistant_list_entry');

      // Get current language.
      $langcode = $this->languageManager->getCurrentLanguage()->getId();

      /** @var \Drupal\site_assistant\Entity\Assistant $translated_active_assistant */
      if ($active_assistant->hasTranslation($langcode) && $translated_active_assistant = $active_assistant->getTranslation($langcode)) {
        $active_assistant = $translated_active_assistant;
      }

      /** @var \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem $assistant_list_entry_reference */
      foreach ($active_assistant->content as $assistant_list_entry_reference) {
        $list_entries[] = $view_builder->view($assistant_list_entry_reference->entity);
      }

      $renderable = [
        '#theme' => 'site_assistant',
        '#list_entries' => $list_entries,
      ];
    }

    return $renderable;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    if ($active_assistant = $this->assistantService->getActiveAssistant()) {
      $contexts = Cache::mergeContexts($contexts, $active_assistant->getCacheContexts());
    }

    return $contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $tags = parent::getCacheTags();
    if ($active_assistant = $this->assistantService->getActiveAssistant()) {
      $tags = Cache::mergeTags($tags, $active_assistant->getCacheTags());
    }

    return $tags;
  }

}
