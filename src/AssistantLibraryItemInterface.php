<?php

namespace Drupal\site_assistant;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a assistant library item entity.
 *
 * @ingroup site_assistant
 */
interface AssistantLibraryItemInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
