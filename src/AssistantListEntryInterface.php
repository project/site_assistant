<?php

namespace Drupal\site_assistant;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Help Section entity.
 *
 * @ingroup site_assistant
 */
interface AssistantListEntryInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
