<?php

namespace Drupal\site_assistant\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Assist List Entry Type.
 *
 * @ingroup site_assistant
 *
 * @ConfigEntityType(
 *   id = "assistant_list_entry_type",
 *   label = @Translation("Assistant List Entry Type"),
 *   bundle_of = "assistant_list_entry",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_prefix = "assistant_list_entry_type",
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 *   handlers = {
 *     "list_builder" = "Drupal\site_assistant\AssistantListEntryTypeListBuilder",
 *     "form" = {
 *       "default" = "Drupal\site_assistant\Form\AssistantListEntryTypeEntityForm",
 *       "add" = "Drupal\site_assistant\Form\AssistantListEntryTypeEntityForm",
 *       "edit" = "Drupal\site_assistant\Form\AssistantListEntryTypeEntityForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer site_assistant",
 *   links = {
 *     "canonical" = "/admin/structure/assistant_list_entry_type/{assistant_list_entry_type}",
 *     "add-form" = "/admin/structure/assistant_list_entry_type/add",
 *     "edit-form" = "/admin/structure/assistant_list_entry_type/{assistant_list_entry_type}/edit",
 *     "delete-form" = "/admin/structure/assistant_list_entry_type/{assistant_list_entry_type}/delete",
 *     "collection" = "/admin/structure/assistant_list_entry_type",
 *   }
 * )
 */
class AssistantListEntryType extends ConfigEntityBundleBase {

  /**
   * The custom block type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The custom block type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The description of the block type.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

}
