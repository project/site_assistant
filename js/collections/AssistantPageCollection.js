/**
 * @file
 * A Backbone Collection for the Assistant page.
 */

(function (Backbone, Drupal) {
  /**
   * Backbone collection for the Assistant page collection.
   *
   * @constructor
   *
   * @augments Backbone.Collection
   */
  Drupal.site_assistant.AssistantPageCollection = Backbone.Collection.extend({});

})(Backbone, Drupal);
