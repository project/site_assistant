/**
 * @file
 * A Backbone view for the Assistant.
 */

(function ($, Backbone, Drupal) {
  Drupal.site_assistant.AssistantView = Backbone.View.extend({
    events: {
      'click div.assistant-list-entry': 'onAssistantListEntryClick',
      'click .site-assistant__toggle': 'onToggleClick',
    },

    /**
     * Backbone view for the Assistant.
     *
     * @constructs
     *
     * @augments Backbone.View
     */
    initialize: function initialize(options) {
      this.pageCollection = options.pageCollection;
      this.window = options.window;
      this.$window = options.$window;

      this.listenTo(this.model, 'change:closed', this.onChangeClosed);
      this.listenTo(this.model, 'change:activePage', this.onChangeActivePage);

      this.onChangeClosed();
      // this.onChangeActivePage();

      this.render();
    },

    /**
     * @inheritdoc
     *
     * @return {Drupal.site_assistant.AssistantView}
     *   The `AssistantView` instance.
     */
    render: function () {
      var halfAnimationTime = this.model.get('pageChangeDuration') / 2;
      this.window.style.transition = 'height ' + halfAnimationTime + 'ms ease-in-out ' + halfAnimationTime + 'ms';
      this.pageCollection.each(function (page) {
        page.attributes.el.style.transition = 'transform ' + halfAnimationTime + 'ms ease-in-out 0ms';
      });

      return this;
    },

    /**
     *
     */
    onChangeActivePage: function () {
      var active_page = this.model.get('activePage');

      if (this.pageCollection.findWhere({id: active_page})) {
        var activePage = this.pageCollection.findWhere({id: active_page}).attributes.el;
        var $activePage = $(activePage);

        // Set assist height to height of current active page.
        this.window.style.height = $activePage.outerHeight() + 'px';
        // this.window.addEventListener('transitionend', this.onAssistWindowTransitionEnd, false);

        // Remove active state from previous active page.
        this.pageCollection.each(function (pageModel) {
          pageModel.attributes.el.classList.remove('assistant-page--active');
          pageModel.attributes.el.style.transform = '';
          pageModel.attributes.el.style.height = '';
          // pageModel.attributes.el.addEventListener('transitionend', this.onPassivePageTransitionEnd, false);
        });

        var offset = 0;
        if ($activePage.parents('.assistant-page').length) {
          var $parents = $activePage.parents('.assistant-page');

          offset = 150;

          $parents.each(function (index, parent) {
            parent.style.transform = 'translateX(-' + offset + 'px)';
          });

          // $parents[0].style.transform = 'translateX(-' + offset + 'px)';
          activePage.style.height = $parents.outerHeight();

          offset = offset * $parents.length;
        }

        activePage.style.transform = 'translateX(' + offset + 'px)';

        activePage.classList.add('assistant-page--active');

        activePage.addEventListener('transitionend', this.onActivePageTransitionEnd, {
          once: true,
        });
      }
    },

    /**
     *
     */
    onActivePageTransitionEnd: function () {
      this.classList.add('assistant-page--active')
    },

    /**
     *
     */
    onChangeClosed: function () {
      this.$el.removeClass('site-assistant--closed');
      if (this.model.get('closed')) {
        this.$el.addClass('site-assistant--closed');
      }
    },

    /**
     *
     * @param e
     */
    onAssistantListEntryClick: function (e) {
      e.preventDefault();
      e.stopPropagation();

      var $assistantListEntry = $(e.currentTarget);
      var $subpage = $assistantListEntry.children('.assistant-page');

      // Only change model state "activePage" if clicked assistant list entry
      // has a subpage.
      if ($subpage.data('assistant-page')) {
        this.model.set('activePage', $subpage.data('assistant-page'));
      }
    },

    /**
     *
     * @param e
     */
    onToggleClick: function (e) {
      this.model.set('closed', false);
      this.model.set('activePage', 0);
    }

  });
})(jQuery, Backbone, Drupal);
