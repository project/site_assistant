/**
 * @file
 * A Backbone view for the Assistant.
 */

(function ($, Backbone, Drupal) {
  Drupal.site_assistant.AssistantNavigationView = Backbone.View.extend({
    events: {
      'click .assistant-navigation__back': 'onBackClick',
      'click .assistant-navigation__close': 'onCloseClick',
    },

    /**
     * Backbone view for the Assistant.
     *
     * @constructs
     *
     * @augments Backbone.View
     */
    initialize: function initialize(options) {
      this.listenTo(this.model, 'change', this.render);

      this.pageCollection = options.pageCollection;
    },

    /**
     * @inheritdoc
     *
     * @return {Drupal.site_assistant.AssistantView}
     *   The `AssistantView` instance.
     */
    render: function () {
      this.$el.html('');
      if (this.model.get('activePage') !== 0) {
        this.$el.append('<div class="assistant-navigation__back"></div>');
      }
      this.$el.append('<div class="assistant-navigation__close"></div>');

      return this;
    },

    onBackClick: function (e) {
      var active_page = this.model.get('activePage');

      if (this.pageCollection.findWhere({id: active_page})) {
        var $activePage = $(this.pageCollection.findWhere({id: active_page}).attributes.el);
        var $parentPage = $activePage.parents('[data-assistant-page]').first();

        if ($parentPage) {
          this.model.set('activePage', $parentPage.data('assistant-page'));
        }
      }

    },

    onCloseClick: function (e) {
      this.model.set('closed', true);
    }

  });
})(jQuery, Backbone, Drupal);
