/**
 * @file
 * A Backbone Model for the Assistant.
 */

(function (Backbone, Drupal) {
  /**
   * Backbone model for the Assistant.
   *
   * @constructor
   *
   * @augments Backbone.Model
   */
  Drupal.site_assistant.AssistantModel = Backbone.Model.extend({

    /**
     * @type {object}
     *
     * @prop pages
     * @prop activePage
     */
    defaults: {
      /**
       * The active assistant page. All other pages should be hidden under
       * normal circumstances.
       *
       * @type {int}
       */
      activePage: -1,

      /**
       * The assistant pages.
       *
       * @type {array}
       */
      pages: [],

      /**
       * If this assistant is closed.
       *
       * @type {boolean}
       */
      closed: true,

      /**
       * The amount of milliseconds a page change animation should take.
       *
       * @type {int}
       */
      pageChangeDuration: 500,
    },

    /**
     * Change the active page to the next.
     */
    nextPage: function () {
      this.set('activePage', this.get('activePage') + 1);
    },

    /**
     * Change the active page to the previous.
     */
    previousPage: function () {
      this.set('activePage', this.get('activePage') - 1);
    }
  });
})(Backbone, Drupal);
