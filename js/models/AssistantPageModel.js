/**
 * @file
 * A Backbone Model for the Assistant page.
 */

(function (Backbone, Drupal) {
  /**
   * Backbone model for the Assistant page.
   *
   * @constructor
   *
   * @augments Backbone.Model
   */
  Drupal.site_assistant.AssistantPageModel = Backbone.Model.extend({

    /**
     * @type {object}
     *
     * @prop id
     */
    defaults: {
      /**
       * The assistant page id.
       *
       * @type {int}
       */
      id: 0,
      /**
       * The assistant page level.
       *
       * @type {int}
       */
      level: 0
    }

  });
})(Backbone, Drupal);
