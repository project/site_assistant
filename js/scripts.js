/**
 * @file
 * Activate the Assistant.
 */

(function ($, Drupal) {
  Drupal.behaviors.site_assistant = {
    attach: function attach(context, settings) {
      $(context).find('.site-assistant').once('site_assistant.assistant').each(function (index) {

        var $siteAssistant = $(this);
        var pageModels = [];

        $siteAssistant.find('[data-assistant-page]').once('site_assistant.assistant_page').each(function (index) {
          pageModels[index] = new Drupal.site_assistant.AssistantPageModel({
            el: this,
            id: $(this).data('assistant-page')
          });
        });

        var model = new Drupal.site_assistant.AssistantModel({
          pages: $(this).find('[data-assistant-page]')
        });

        new Drupal.site_assistant.AssistantNavigationView({
          el: $(this).find('.assistant-navigation')[0],
          pageCollection: new Drupal.site_assistant.AssistantPageCollection(pageModels),
          model: model
        });
        new Drupal.site_assistant.AssistantView({
          el: this,
          $window: $(this).find('.site-assistant__window'),
          window: $(this).find('.site-assistant__window')[0],
          pageCollection: new Drupal.site_assistant.AssistantPageCollection(pageModels),
          model: model
        });
      });
    }
  };

  Drupal.site_assistant = Drupal.site_assistant || {
    models: {},

    views: {}
  };
})(jQuery, Drupal);
